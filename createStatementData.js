export default function createStatementData(invoice, plays) {
    const result = {};
    result.customer = invoice.customer;
    result.performances = invoice.performances.map(enrichPerformance);
    result.totalAmount = totalAmount(result); /*Internalizar variável atravez da chamada da função totalAmount(), pela falta de necessidade de existir uma varivel totalAmount pois só se usa a informação uma vez; 
        Arquivo: 01 app original.js
        linha 2: let totalAmount = 0;
    */
    result.totalVolumeCredits = totalVolumeCredits(result);/*Internalizar variável atravez da chamada da função totalVolumeCredits(), pela falta de necessidade de existir uma varivel totalVolumeCredits pois só se usa a informação uma vez; 
        Arquivo: 01 app original.js
        linha 3: let volumeCredits = 0;
    */
    return result;
    function enrichPerformance(aPerformance) {
        const calculator = createPerformanceCalculator(aPerformance, playFor(aPerformance));
        const result = Object.assign({}, aPerformance);
        result.play = calculator.play; result.amount = calculator.amount;
        result.volumeCredits = calculator.volumeCredits;
        return result;
    }

    function playFor(aPerformance) {/* Extração de função do const play para retirar uma variável temporaria do codigo e facilitar na legibilidade e manutenção.
        Arquivo: 01 app original.js
        linha 9: const play = plays[perf.playID];
        
    */
        return plays[aPerformance.playID]
    }

    /* Para o entendimento do codigo atual é necessário ter um pequeno histórico de algumas alteraçoẽs que o bloco de codigo abaixo recebeu.

    Arquivo: 01 app original.js
    Linhas 8-38: 

    for (let perf of invoice.performances) {
    const play = plays[perf.playID];
    let thisAmount = 0;

    switch (play.type) {
    case "tragedy":
      thisAmount = 40000;
      if (perf.audience > 30) {
        thisAmount += 1000 * (perf.audience - 30);
      }
      break;
    case "comedy":
      thisAmount = 30000;
      if (perf.audience > 20) {
        thisAmount += 10000 + 500 * (perf.audience - 20);
      }
      thisAmount += 300 * perf.audience;
      break;
    default:
        throw new Error(`unknown type: ${play.type}`);
    }

    // add volume credits
    volumeCredits += Math.max(perf.audience - 30, 0);
    // add extra credit for every ten comedy attendees
    if ("comedy" === play.type) volumeCredits += Math.floor(perf.audience / 5);

    // print line for this order
    result += `  ${play.name}: ${format(thisAmount/100)} (${perf.audience} seats)\n`;
    totalAmount += thisAmount;
  }

    Como o objetivo do switch é calcular o valor total cobrado é possivel atravez do metodo da extração de função criar uma função para assumir essa tarefa.

    function amountFor(perf, play) {
        let thisAmount = 0;
        switch (play.type) {
            case "tragedy":
                thisAmount = 40000;
                if (perf.audience > 30) {
                    thisAmount += 1000 * (perf.audience - 30);
                }
                break;
            case "comedy":
                thisAmount = 30000;
                if (perf.audience > 20) {
                    thisAmount += 10000 + 500 * (perf.audience - 20);
                }
                thisAmount += 300 * perf.audience;
                break;
            default:
                throw new Error(`unknown type: ${play.type}`);
        }
        return thisAmount;
    }
    Ficamos com os for da seguinde forma:

    for (let perf of invoice.performances) {
        const play = plays[perf.playID];
        let thisAmount = amountFor(perf, play);
        // soma créditos por volume
        volumeCredits += Math.max(perf.audience - 30, 0);
        // soma um crédito extra para cada
        // dez espectadores de comédia
        if ("comedy" === play.type) volumeCredits += Math.floor(perf.audience / 5);
        // exibe a linha para esta requisição
        result += ` ${play.name}: ${format(thisAmount / 100)} (${perf.audience} seats)\n`;
        totalAmount += thisAmount;
    }

    Atraves da refatoração de diversas partes do codigo com metodos já citados acima ficamos com este bloco de codigo:
 
   for (let perf of invoice.performances) {
        volumeCredits += volumeCreditsFor(perf);
        // exibe a linha para esta requisição
        result += ` ${playFor(perf).name}:
        ${usd(amountFor(perf))} (${perf.audience} seats)\n`;
        totalAmount += amountFor(perf);
    }

    No livro é recomendado que exista uma divisão em laços caso dentro de um laço de repetição existam duas ou mais tarefas distintas rodando, pois facilita o uso e a manutenção destes laços.

    for (let perf of invoice.performances) {
        // exibe a linha para esta requisição
        result += ` ${playFor(perf).name}:
        ${usd(amountFor(perf))} (${perf.audience} seats)\n`;
        totalAmount += amountFor(perf);
    }

    for (let perf of invoice.performances) {
        volumeCredits += volumeCreditsFor(perf);
    }
    */

    function totalAmount(data) {/* Extração de função do for abaixo para facilitar a legibilidade do codigo.
       
        for (let perf of invoice.performances) {
            // exibe a linha para esta requisição
            result += ` ${playFor(perf).name}:
            ${usd(amountFor(perf))} (${perf.audience} seats)\n`;
            totalAmount += amountFor(perf);
        }
    */
        return data.performances
            .reduce((total, p) => total + p.amount, 0);
    }
    function totalVolumeCredits(data) {/* Extração de função do for abaixo para facilitar a legibilidade do codigo.
     
        for (let perf of invoice.performances) {
            volumeCredits += volumeCreditsFor(perf);
        }
    */
        return data.performances
            .reduce((total, p) => total + p.volumeCredits, 0);
    }
}
function createPerformanceCalculator(aPerformance, aPlay) { /*A função a abaixo passou pelo processo de substituir condicional por polimorfismo, porque em casos de logicas condicionais complexas é preferivel encontrar maneiras de deixar mais claro a sua representação e usar classes e polimorfismo pode gerar uma divisão mais explicita.

function amountFor(perf, play) {
        let thisAmount = 0;
        switch (play.type) {
            case "tragedy":
                thisAmount = 40000;
                if (perf.audience > 30) {
                    thisAmount += 1000 * (perf.audience - 30);
                }
                break;
            case "comedy":
                thisAmount = 30000;
                if (perf.audience > 20) {
                    thisAmount += 10000 + 500 * (perf.audience - 20);
                }
                thisAmount += 300 * perf.audience;
                break;
            default:
                throw new Error(`unknown type: ${play.type}`);
        }
        return thisAmount;
    }
*/
    switch (aPlay.type) {
        case "tragedy": return new TragedyCalculator(aPerformance, aPlay);
        case "comedy": return new ComedyCalculator(aPerformance, aPlay);
        case "musical": return new MusicalCalculator(aPerformance, aPlay);
        default:
            throw new Error(`unknown type: ${aPlay.type}`);
    }
}
class PerformanceCalculator {
    constructor(aPerformance, aPlay) {
        this.performance = aPerformance;
        this.play = aPlay;
    }
    get amount() {
        throw new Error('subclass responsibility');
    }
    get volumeCredits() {
        return Math.max(this.performance.audience - 30, 0);
    }
}
class TragedyCalculator extends PerformanceCalculator {
    get amount() {
        let result = 40000;
        if (this.performance.audience > 30) {
            result += 1000 * (this.performance.audience - 30);
        }
        return result;
    }
}
class ComedyCalculator extends PerformanceCalculator {
    get amount() {
        let result = 30000;
        if (this.performance.audience > 20) {
            result += 10000 + 500 * (this.performance.audience - 20);
        }
        result += 300 * this.performance.audience;
        return result;
    }
    get volumeCredits() {
        return super.volumeCredits + Math.floor(this.performance.audience / 5);
    }
}

class MusicalCalculator extends PerformanceCalculator {
    get amount() {
        let result = 30000;
        if (this.performance.audience > 25) {
            result += 10000 + 500 * (this.performance.audience - 25);
        }
        result += 450 * this.performance.audience;
        return result;
    }
}